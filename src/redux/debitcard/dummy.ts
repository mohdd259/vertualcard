import { images } from "../../utils";
import { cardFeaturesTypes } from './types'

export const debitCardFeatures = [
  {
    icon: images['image_top_up'],
    title: 'Top-up account',
    description: 'Deposit money to your account to use with card',
    id: cardFeaturesTypes.TOP_UP_ACCOUNT,
  },
  {
    icon: images['image_weekly_limit'],
    title: 'Weekly spending limit',
    description: 'Your weekly spending limit is S$ 5,000',
    isToggle: false,
    onTap: {
      redirectType: 'SCREEN',
      screenName: 'SpendingLimit',
    },
    id: cardFeaturesTypes.WEEKLY_LIMIT,
  },
  {
    icon: images['image_freeze_card'],
    title: 'Freeze card',
    description: 'Your debit card is currently active',
    isToggle: false,
    id: cardFeaturesTypes.FREEZE_CARD,
  },
  {
    icon: images['image_new_card'],
    title: 'Get a new card',
    description: 'This deactivates your current debit card',
    id: cardFeaturesTypes.GET_NEW_CARD,
  },
  {
    icon: images['image_deactivate_card'],
    title: 'Deactivated cards',
    description: 'Your previously deactivated cards',
    id: cardFeaturesTypes.DEACTIVATE_CARD,
  },
];

// import { debitCardFeatures } from "./dummy";

export enum cardFeaturesTypes{
  WEEKLY_LIMIT = 'weekly-limit',
  FREEZE_CARD = 'freeze-card',
  TOP_UP_ACCOUNT = 'top-up-account',
  GET_NEW_CARD = 'get-new-card',
  DEACTIVATE_CARD = 'deactivate-card',
}

export interface DebitcardFeatures {
  icon: any;
  title: string;
  description: string;
  isToggle?: string;
  onTap?: any;
  id?: string;
}

export interface MokeDataTypes {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
}

export interface DebitcardData {
  debitcardNo: string;
  expiryDate: string;
  cvvNo: string;
  showCardNumber: boolean;
  userBalance: number;
  isWeeklyLimit: boolean;
  weeklyLimit: number;
  userSpentBalance: number;
  isFreezeCard: boolean;
  debitCardFeatures: Array<DebitcardFeatures>;
  mokeData: MokeDataTypes;
}

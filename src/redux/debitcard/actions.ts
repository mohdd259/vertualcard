import * as actions from '../actions';
import {put} from 'redux-saga/effects';
import {Api} from '../../services/accountApi/network';

export function* mokeApiRequest() {
  try {
    yield Api.getProduct();
    yield put(actions.mokeApiSuccess({}));
  } catch (e) {
    yield put(actions.mokeApiError());
  }
}

import {handleActions} from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../constants';
import {debitCardFeatures} from './dummy';
import {cardFeaturesTypes, DebitcardData} from './types';

const initialState: DebitcardData = {
  debitcardNo: '5647341124132020',
  expiryDate: '12/20',
  cvvNo: '456',
  showCardNumber: false,
  userBalance: 3000,
  isWeeklyLimit: false,
  weeklyLimit: 5000,
  userSpentBalance: 345,
  isFreezeCard: false,
  debitCardFeatures,

  mokeData: {
    isSuccess: false,
    isError: false,
    isLoading: false,
    data: {},
  },
};

const showCardDetailsRequest = (state: DebitcardData, action: any) => {
  return update(state, {
    showCardNumber: {$set: action.payload},
  });
};

const setWeeklyLimitRequest = (state: DebitcardData, action: any) => {
  return update(state, {
    weeklyLimit: {$set: action.payload},
  });
};

const setWeeklyLimitStatusRequest = (state: DebitcardData, action: any) => {
  let dcFeatures = [...state.debitCardFeatures];
  const itemIndex = dcFeatures.findIndex(
    a => a.id === cardFeaturesTypes.WEEKLY_LIMIT,
  ); // we can skip this line if we directly get index in payload

  dcFeatures[itemIndex].isToggle =
    action.payload !== undefined
      ? action.payload
      : !dcFeatures[itemIndex].isToggle;

  return update(state, {
    debitCardFeatures: {$set: dcFeatures},
    isWeeklyLimit: {$set: dcFeatures[itemIndex].isToggle},
  });
};

const setCardStatusRequest = (state: DebitcardData, action: any) => {
  let dcFeatures = [...state.debitCardFeatures];
  const itemIndex = dcFeatures.findIndex(
    a => a.id === cardFeaturesTypes.FREEZE_CARD,
  ); // we can skip this line if we directly get index in payload

  dcFeatures[itemIndex].isToggle =
    action.payload !== undefined
      ? action.payload
      : !dcFeatures[itemIndex].isToggle;

  return update(state, {
    debitCardFeatures: {$set: dcFeatures},
  });
};

const mokeApiRequest = (state: DebitcardData, action: any) => {
  return update(state, {
    mokeData: {
      isLoading: {$set: true},
      isSuccess: {$set: false},
      isError: {$set: false},
      data: {$set: {}},
    },
  });
};

const mokeApiSuccess = (state: DebitcardData, action: any) => {
  return update(state, {
    mokeData: {
      isLoading: {$set: false},
      isSuccess: {$set: true},
      isError: {$set: false},
      data: {$set: action.payload},
    },
  });
};

const mokeApiError = (state: DebitcardData) => {
  return update(state, {
    mokeData: {
      isLoading: {$set: false},
      isSuccess: {$set: false},
      isError: {$set: true},
      data: {$set: {}},
    },
  });
};

export default handleActions(
  {
    [constants.SHOW_CARD_DETAILS_REQUEST]: showCardDetailsRequest,
    [constants.SET_WEEKLY_LIMIT_REQUEST]: setWeeklyLimitRequest,
    [constants.SET_CARD_STATUS_REQUEST]: setCardStatusRequest,
    [constants.SET_WEEKLY_LIMIT_STATUS_REQUEST]: setWeeklyLimitStatusRequest,

    [constants.MOCK_API_REQUEST]: mokeApiRequest,
    [constants.MOCK_API_SUCCESS]: mokeApiSuccess,
    [constants.MOCK_API_ERROR]: mokeApiError,



  },
  initialState,
);

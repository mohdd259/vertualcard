import { combineReducers } from "redux";
import debitcard from './debitcard/reducer';

const appReducer = combineReducers({
  debitcard,
});

export type RootState = ReturnType<typeof appReducer>;

export default appReducer;

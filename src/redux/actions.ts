import {createAction} from 'redux-actions';
import * as constants from './constants';

export const showCardDetailsRequest = createAction(constants.SHOW_CARD_DETAILS_REQUEST);
export const setWeeklyLimitRequest = createAction(constants.SET_WEEKLY_LIMIT_REQUEST);
export const setCardStatusRequest = createAction(constants.SET_CARD_STATUS_REQUEST);
export const setWeeklyLimitStatusRequest = createAction(constants.SET_WEEKLY_LIMIT_STATUS_REQUEST);

export const mokeApiRequest = createAction(constants.MOCK_API_REQUEST);
export const mokeApiSuccess = createAction(constants.MOCK_API_SUCCESS);
export const mokeApiError = createAction(constants.MOCK_API_ERROR);

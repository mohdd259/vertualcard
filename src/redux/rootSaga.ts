import {takeLatest, all} from 'redux-saga/effects';
import * as constants from './constants';
import {mokeApiRequest} from './debitcard/actions';

export function* watchActions() {
  yield takeLatest(constants.MOCK_API_REQUEST, mokeApiRequest);
}

export default function* rootSaga() {
  yield all([watchActions()]);
}

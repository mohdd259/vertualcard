export const colors = {
    SHADOW_COLOR: '#000',
    
    BLUE00: "#00DBFF",
    LIGHT_BLUE_00 :"#00A2BC",
    BLUE05: 'rgba(0, 219, 255, .07)',
    BLUE0CA: '#0C365A',

    BLACK22: '#222222',
    
    GREEN00: '#01D167',
    GREEN00AC: '#00AC6F',
    GREEND9: '#D9FFF1',
    GREEN00A6: '#00a66b',
    GREENE5F: '#e5faf0',

    WHITE: "#FFFFFF",
    WHITEF5: "#F5F5F5",

    BLACK: "#000000",
    BLACK05: 'rgba(0, 0, 0, .5)',
    
    BORDER_COLOR: "#31403B",

    YELLOW11: 'rgba(253, 201, 66, .11)',
    YELLOW: '#ffb039',
    YELLOWFF: '#FFD200',

    REDFF: '#ff8182',
    REDFFE: '#ffefef',
    RED1D: '#1d1d20',

    GREY78: "#787878",
    GREYE2: '#E2E2E2',
    GREYCF: '#CFCFCF',
    GREYF4: '#f4f4f4',
    GREYD1: '#d1d1d1',
    GREY8A: '#8A928F',
    GREYDD: '#DDDDDD',
    GREYEE: '#EEEEEE',
    GREYB4: '#b4b4b5',
    GREY222: '#22222233',
    GREY226: '#22222266',

    RED: 'red',

    TRANSPARENT: 'transparent'
}
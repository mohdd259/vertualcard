import React from 'react'
import { Image, Text, TextInput } from 'react-native'
import { fonts } from './fonts'

const defaultTextStyle = {
  fontFamily: fonts.REGULAR,
  fontSize: 14,
  fontWeight: 'normal',
}

// INFO: https://react-native-elements.github.io/react-native-elements/docs/customization.html#using-themeprovider

const theme = {
  Button: {
    titleStyle: {
      ...defaultTextStyle, // Otherwise buttons won't have same font
    },
  },
}

export const initTheme = async (callback = null) => {

  Image['defaultProps'] = Image['defaultProps'] || {}

  Image['defaultProps'].fadeDuration = 0

  Text['defaultProps'] = Text['defaultProps'] || {}

  Text['defaultProps'].allowFontScaling = false

  TextInput['defaultProps'] = Text['defaultProps'] || {}

  TextInput['defaultProps'].allowFontScaling = false

  const originalTextRender = Text['render']

  Text['render'] = function (...args) {
    const origin = originalTextRender.call(this, ...args)
    return React.cloneElement(origin, { style: [defaultTextStyle, origin.props.style] })
  }

  return theme
}

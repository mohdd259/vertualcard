export const fonts = {
    LIGHT: 'Montserrat-Light',
    REGULAR: 'Montserrat-Regular',
    MEDIUM: 'Montserrat-Medium',
    SEMI_BOLD: 'Montserrat-SemiBold',
    BOLD: 'Montserrat-Bold',
    EXTRA_BOLD: 'Montserrat-ExtraBold',
    ITALIC: 'Montserrat-Italic',
    AVENIC: 'Avenir-Next-Font'
}
import { View, Text } from "react-native";
import React from "react";
import { CustomFastImage } from "../custom-fast-image";
import { styles } from "./debit-card-feature-item.styles";
import { CustomToggleButton } from "../custom-toggle-button";
import { useNavigation } from "@react-navigation/native";
import { setCardStatusRequest, setWeeklyLimitStatusRequest } from "../../redux/actions";
import { useDispatch } from "react-redux";
import { cardFeaturesTypes, DebitcardFeatures } from "../../redux/debitcard/types";

export const DebitCardFeatureItem: React.FC<DebitcardFeatures> = props => {
  const {title, description, icon, isToggle, id, onTap} = props;
  const isRenderToggle = props.hasOwnProperty('isToggle');
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const onToggle = () => {
    switch (id) {
      case cardFeaturesTypes.WEEKLY_LIMIT:
        if (isToggle) {
          dispatch(setWeeklyLimitStatusRequest(false));
        } else if (onTap) {
          navigation.navigate(onTap.screenName);
        }
        return;
      case cardFeaturesTypes.FREEZE_CARD:
        dispatch(setCardStatusRequest());
        return;
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.infoView}>
        <CustomFastImage source={icon} style={styles.icon} />
        <View>
          <Text style={styles.titleText}>{title}</Text>
          <Text style={styles.descriptionText}>{description}</Text>
        </View>
      </View>
      {isRenderToggle && (
        <CustomToggleButton onToggle={onToggle} isActive={isToggle} />
      )}
    </View>
  );
}

import { ImageStyle, StyleSheet, TextStyle, ViewStyle } from "react-native";
import { colors, fonts } from "../../theme";


export const styles = StyleSheet.create({
  icon: {
    width: 32,
    height: 32,
    marginRight: 10
  } as ImageStyle,

  container: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 40,
  } as ViewStyle,

  titleText: {
    fontSize: 13,
    fontFamily: fonts.MEDIUM,
    color: colors.BLACK22,
  } as TextStyle,

  infoView: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  } as ViewStyle,

  descriptionText: {
    fontSize: 12,
    fontFamily: fonts.REGULAR,
    color: colors.GREYB4,
  } as TextStyle,
});

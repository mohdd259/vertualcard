import FastImage from 'react-native-fast-image';
import React from 'react';

export function CustomFastImage(props: any) {
  return <FastImage {...props} />;
}

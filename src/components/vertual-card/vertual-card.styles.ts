import {ImageStyle, StyleSheet, TextStyle, ViewStyle} from 'react-native';
import {colors, fonts} from '../../theme';
import {Device} from '../../utils';

const CARD_HEIGHT = 220;

export const styles = StyleSheet.create({
  cardView: {
    height: CARD_HEIGHT,
    backgroundColor: colors.GREEN00,
    width: Device.screenWidth - 40,
    alignSelf: 'center',
    borderRadius: 12,
    zIndex: 10,
    marginTop: 33,
  } as ViewStyle,

  aspireLogo: {
    width: 74,
    height: 21,
    alignSelf: 'flex-end',
    marginRight: 24,
    marginTop: 24,
  } as ImageStyle,

  visaLogo: {
    width: 59,
    height: 20,
    alignSelf: 'flex-end',
    marginRight: 24,
    marginBottom: 24,
  } as ImageStyle,

  container: {
    flex: 1,
    paddingHorizontal: 20,
  } as ViewStyle,

  nameOnCardText: {
    fontFamily: fonts.BOLD,
    fontSize: 22,
    color: colors.WHITE,
    marginVertical: 24,
  } as TextStyle,

  cardNoWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  } as ViewStyle,

  cardNoText: {
    fontFamily: fonts.SEMI_BOLD,
    fontSize: 14,
    color: colors.WHITE,
    marginRight: 20,
    letterSpacing: 3,
  } as TextStyle,

  cardDateView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  } as ViewStyle,

  cardDateText: {
    fontFamily: fonts.SEMI_BOLD,
    fontSize: 13,
    color: colors.WHITE,
    marginRight: 20,
    letterSpacing: 3,
  } as TextStyle,

  thruText: {
    fontFamily: fonts.SEMI_BOLD,
    fontSize: 13,
    color: colors.WHITE,
    marginRight: 20,
    letterSpacing: 0,
    fontWeight: '600',
  } as TextStyle,

  cardCvvText: {
    fontFamily: fonts.SEMI_BOLD,
    fontSize: 13,
    color: colors.WHITE,
    marginRight: 20,
    letterSpacing: 2,
    textAlignVertical: 'center',
    includeFontPadding: false,
  } as TextStyle,

  hideCardNumberView: {
    position: 'absolute',
    top: 0,
    right: 20,
    width: 151,
    height: 44,
    backgroundColor: colors.WHITE,
    zIndex: 1,
    borderRadius: 5,
  } as ViewStyle,

  vertualCardWrapper: {} as ViewStyle,

  eyeWrapper: {
    height: 33,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  } as ViewStyle,

  hideText: {
    fontSize: 12,
    fontFamily: fonts.SEMI_BOLD,
    color: colors.GREEN00,
  } as TextStyle,

  eyeIcon: {
    width: 12,
    height: 12,
    marginRight: 5,
  } as ImageStyle,

  placeholderView: {
    width: 8,
    height: 8,
    borderRadius: 8,
    backgroundColor: colors.WHITE,
    marginRight: 3,
  } as ViewStyle,

  placeholderWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 20,
  } as ViewStyle,

  cvvView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  } as ViewStyle,
});

import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import { styles } from "./vertual-card.styles";
import { CustomFastImage } from "../custom-fast-image";
import { images } from "../../utils";
import { useDispatch, useSelector } from "react-redux";
import { showCardDetailsRequest } from "../../redux/actions";
import { DebitcardData } from "../../redux/debitcard/types";
import { RootState } from "../../redux/rootReducer";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

interface VertualCardProps {}

export const VertualCard: React.FC<VertualCardProps> = (props) => {
  const dispatch = useDispatch();
  const debitcard: DebitcardData = useSelector((state: RootState) => state.debitcard);
  const { debitcardNo, expiryDate, cvvNo, showCardNumber } = debitcard;

  const handleToggleCardNumber = () => {
    dispatch(showCardDetailsRequest(!showCardNumber));
  };

  const debitcardNoInGroup = debitcardNo?.replace(/(\d{4}(?!\s))/g, "$1 ")?.split(" ")?.splice(0, 4) || [];

  const cardNumberPlaceHolder = (index: number) => {
    return (
      <View key={String(index)} style={styles.placeholderWrapper}>
        {Array(4)
          .fill(0)
          .map((a, index) => {
            return <View key={String(index)} style={styles.placeholderView} />;
          })}
      </View>
    );
  };

  const cardCvvPlaceholder = () => {
    return Array(cvvNo?.split('').length)
      .fill(0)
      .map((a, index) => {
        return <FontAwesome5 size={9} key={String(index)} name={'asterisk'} />;
      });
  };

  return (
    <View style={styles.vertualCardWrapper} >
      <View style={styles.hideCardNumberView}>
        <TouchableOpacity activeOpacity={0.6} onPress={handleToggleCardNumber} style={styles.eyeWrapper}>
          <CustomFastImage
            source={
              images[!showCardNumber ? "image_show_eye" : "image_hide_eye"]
            }
            style={styles.eyeIcon}
          />
          <Text style={styles.hideText}>{showCardNumber ? "Hide" : "Show"} card number</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.cardView}>
        <CustomFastImage source={images["image_aspire_logo"]} style={styles.aspireLogo} />
        <View style={styles.container}>
          <Text style={styles.nameOnCardText}>Mark Henry</Text>
          <View style={styles.cardNoWrapper}>
            {debitcardNoInGroup.map((no: string, i: number) => {
              return (!showCardNumber && i !== debitcardNoInGroup.length - 1) ? cardNumberPlaceHolder(i) : (
                <Text key={String(i)} style={styles.cardNoText}>{no}</Text>
              );
            })}
          </View>
          <View style={styles.cardDateView}>
            <Text style={styles.cardDateText}><Text style={styles.thruText}>Thru:</Text> {expiryDate}</Text>
            <View style={styles.cvvView}>
              <Text style={styles.thruText}>CVV:</Text>
              <Text style={styles.cardCvvText}>{showCardNumber ? cvvNo : cardCvvPlaceholder()}</Text>
            </View>
          </View>
        </View>
        <CustomFastImage source={images["image_visa_logo"]} style={styles.visaLogo} />
      </View>
    </View>
  );
}

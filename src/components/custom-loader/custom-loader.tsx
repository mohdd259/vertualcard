import React from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {styles} from './custom-loader.styles';
import {CardView} from '../card-view';

interface CustomLoaderProps {
  isLoading?: boolean;
  isBackDrop?: boolean;
  isAbsolute?: boolean;
}

export const CustomLoader: React.FC<CustomLoaderProps> = props => {
  const {isLoading, isBackDrop, isAbsolute} = props;

  if (!isLoading) {
    return null;
  }

  return (
    <View
      style={[
        styles.container,
        isAbsolute && {...StyleSheet.absoluteFill},
        isBackDrop && styles.backdropView,
      ]}>
      <CardView style={styles.loaderWrapper}>
        <ActivityIndicator size="large" />
        <Text style={styles.loadingText}>Loading...</Text>
      </CardView>
    </View>
  );
};

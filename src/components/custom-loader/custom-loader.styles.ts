import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../theme';

export const styles = StyleSheet.create({
  absolute: {},

  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100,
  },

  loadingText: {
    fontFamily: fonts.MEDIUM,
    marginTop: 15,
    color: colors.BORDER_COLOR,
    fontSize: 12,
  },

  loaderWrapper: {
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },

  backdropView: {
    backgroundColor: colors.BLACK05,
  },
});

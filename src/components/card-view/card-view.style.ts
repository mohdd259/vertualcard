import {StyleSheet} from 'react-native';
import {colors} from '../../theme';

export const styles = StyleSheet.create({
  Container: {
    shadowColor: colors.BLACK,
    borderRadius: 6,
    backgroundColor: colors.WHITE,
  },
  Disabled: {
    borderWidth: 1,
    borderColor: colors.GREYE2,
  },
  ContainerWithShadow: {
    borderRadius: 6,
    backgroundColor: colors.WHITE,

    shadowColor: colors.SHADOW_COLOR,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 6,

    elevation: 4,
  },
});

// ANIMATION
export const shadow = animatedValue => ({
  shadowColor: colors.SHADOW_COLOR,
  shadowOpacity: animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0.25, 0.34],
  }),
  shadowRadius: animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [3.84, 6.27],
  }),
  shadowOffset: {
    width: 0,
    height: animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [2, 5],
    }),
  },

  elevation: animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [3, 10],
  }),
});

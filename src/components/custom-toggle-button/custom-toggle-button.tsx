import React, {useState} from 'react';
import {colors} from '../../theme';
import ToggleSwitch from 'toggle-switch-react-native';

interface CustomToggleButtonProps{
  isActive?: boolean;
  onToggle?: void;
}

export const CustomToggleButton: React.FC<CustomToggleButtonProps> = props => {
  const {isActive, onToggle} = props;
  const [active, setActive] = useState(isActive || false);

  const handleToggle = () => {
    onToggle ? onToggle() : setActive(!active);
  };

  return (
    <ToggleSwitch
      isOn={isActive || active}
      onColor={colors.GREEN00}
      offColor={colors.GREYEE}
      size="small"
      onToggle={handleToggle}
    />
  );
}

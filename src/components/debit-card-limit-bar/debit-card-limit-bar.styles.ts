import { StyleSheet, TextStyle, ViewStyle } from "react-native";
import { colors, fonts } from "../../theme";
import { Device } from "../../utils";

export const BAR_WIDTH = Device.screenWidth - 40;


export const styles = StyleSheet.create({
  container: {
    marginTop: 25
  } as ViewStyle,

  infoWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  } as ViewStyle,

  debitCardLimitText: {
    fontSize: 12,
    fontFamily: fonts.MEDIUM,
    color: colors.BLACK22
  } as TextStyle,

  minLimitText: {
    fontSize: 13,
    fontFamily: fonts.SEMI_BOLD,
    color: colors.GREEN00
  } as TextStyle,

  maxLimitText: {
    fontFamily: fonts.MEDIUM,
    fontSize: 13,
    color: colors.GREY222
  } as TextStyle,

  helperView: {
    height: 13,
    width: 1,
    backgroundColor: colors.GREY222,
    marginHorizontal: 7
  } as ViewStyle,

  limitView: {
    flexDirection: "row",
    alignItems: "center"
  } as ViewStyle,

  mainBarView: {
    width: BAR_WIDTH,
    backgroundColor: colors.GREENE5F,
    height: 15,
    marginTop: 6,
    borderRadius: 20,
    overflow: "hidden"
  } as ViewStyle,

  filledView: {
    height: "100%",
    backgroundColor: colors.GREEN00,
    justifyContent: "flex-end",
    flexDirection: "row"
  } as ViewStyle,

  noveView: {
    height: "100%",
    width: 20,
    transform: [
      { rotateX: "-40deg" }, // skew doesn't work properly on android, it's just a workaround of skew(skewX: -18deg)
      { rotateY: "-310deg" },
      { rotateZ: "-40deg" }
    ],
    backgroundColor: colors.GREEN00,
    marginRight: -10
  } as ViewStyle
});

import { View, Text } from "react-native";
import React from "react";
import { styles, BAR_WIDTH } from "./debit-card-limit-bar.styles";
import { useSelector } from "react-redux";
import { convertNumberToCommasKLC } from "../../utils";
import { DebitcardData } from "../../redux/debitcard/types";
import { RootState } from "../../redux/rootReducer";

export function DebitCardLimitBar() {
  const debitcard: DebitcardData = useSelector((state: RootState) => state.debitcard);
  const {userSpentBalance, weeklyLimit, isWeeklyLimit} = debitcard;

  if (!isWeeklyLimit) return null;

  return (
    <View style={styles.container}>
      <View style={styles.infoWrapper}>
        <Text style={styles.debitCardLimitText}>Debit card spending limit</Text>
        <View style={styles.limitView}>
          <Text style={styles.minLimitText}>${convertNumberToCommasKLC(userSpentBalance)}</Text>
          <View style={styles.helperView} />
          <Text style={styles.maxLimitText}>${convertNumberToCommasKLC(weeklyLimit)}</Text>
        </View>
      </View>
      <View style={styles.mainBarView}>
        <View style={[styles.filledView, { width: BAR_WIDTH * (userSpentBalance / weeklyLimit) }]}>
          <View style={styles.noveView} />
        </View>
      </View>
    </View>
  );
}

import React, {useState} from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Input} from 'react-native-elements';
import {colors} from '../../theme';
import {CustomInputProps} from './custom-input.props';
import {styles} from './custom-input.styles';

const onlyNumericRegex = /^[0-9]+$/;

export const CustomInput: React.FC<CustomInputProps> = props => {
  const {
    labelStyle = {},
    style = {},
    placeholder = '',
    containerStyle = {},
    name = '',
    inputStyle = {},
    inputContainerStyle = {},
    onChangeText = () => null,
    errorMeessage = '',
    secureTextEntry = false,
    onlyNumericAllowed = false,
    allowKeywords = [],
    ...restProps
  } = props;

  let isRenderIcon = false;

  const [isPasswordSecurity, setPasswordSecurity] = useState(secureTextEntry);

  const getRestProps = () => {
    if (secureTextEntry) {
      isRenderIcon = true;
      restProps.rightIcon = (
        <TouchableOpacity
          onPress={() => setPasswordSecurity(!isPasswordSecurity)}>
          <Icon
            name={isPasswordSecurity ? 'eye' : 'eye-off'}
            color={colors.GREY78}
            size={18}
          />
        </TouchableOpacity>
      );
    }
    return restProps;
  };

  const isAllowedKeywords = (value: string) => {
    return allowKeywords?.filter(a => String(value)?.includes(a)).length > 0;
  };

  const handleOnChangeText = (value: string) => {
    if (onlyNumericAllowed) {
      (onlyNumericRegex.test(value) || value === '' || isAllowedKeywords(value)) &&
        onChangeText(value, name);
    } else {
      onChangeText(value, name);
    }
  };

  return (
    <Input
      {...getRestProps()}
      placeholder={placeholder}
      labelStyle={[styles.labelStyle, labelStyle]}
      style={[styles.input, style]}
      containerStyle={[styles.containerStyle, containerStyle]}
      inputContainerStyle={[styles.inputContainerStyle, inputContainerStyle]}
      inputStyle={[styles.inputStyle, inputStyle]}
      secureTextEntry={isPasswordSecurity}
      keyboardType={onlyNumericAllowed ? 'number-pad' : 'default'}
      onChangeText={handleOnChangeText}
      errorMessage={errorMeessage}
    />
  );
};


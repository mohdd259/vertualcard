import {ViewStyle} from 'react-native';

export interface CustomInputProps {
  labelStyle?: ViewStyle;
  style?: ViewStyle;
  placeholder?: string;
  containerStyle?: ViewStyle;
  name?: string;
  inputStyle?: ViewStyle;
  inputWrapper?: ViewStyle;
  inputContainerStyle?: ViewStyle;
  onChangeText?: (value?: string, name?: string) => void;
  errorMeessage?: string;
  secureTextEntry?: boolean;
  onlyNumericAllowed?: boolean;
  allowKeywords?: [string];
}

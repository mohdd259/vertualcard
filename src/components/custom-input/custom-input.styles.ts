import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../theme';

export const styles = StyleSheet.create({
  labelStyle: {},

  input: {
    fontSize: 14,
    fontFamily: fonts.REGULAR,
    marginBottom: -5,
  },

  containerStyle: {
    width: '100%',
    height: 40,
    paddingHorizontal: 0,
  },

  inputContainerStyle: {
    borderColor: colors.GREY222,
    borderBottomWidth: 0.3,
    height: 40,
  },

  inputStyle: {},
});

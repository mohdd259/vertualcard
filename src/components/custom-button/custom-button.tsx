import React from 'react';
import {ViewStyle} from 'react-native';
import {Button} from 'react-native-elements';
import {styles} from './custom-button.styles';

interface CustomButtonProps {
  title?: string;
  isLoading?: boolean;
  disabled?: boolean;
  buttonStyle?: ViewStyle;
  activeOpacity?: number;
  titleStyle?: ViewStyle;
}

export const CustomButton: React.FC<CustomButtonProps> = props => {
  const {
    title = 'Submit',
    isLoading = false,
    disabled = false,
    buttonStyle = {},
    activeOpacity = 0.6,
    titleStyle = {},
    ...restProps
  } = props;
  return (
    <Button
      {...restProps}
      buttonStyle={[styles.button, buttonStyle]}
      title={title}
      loading={isLoading}
      disabled={disabled}
      activeOpacity={activeOpacity}
      titleStyle={[styles.titleStyle, titleStyle]}
    />
  );
};

import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../theme';

export const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.GREEN00,
    height: 56,
    borderRadius: 100,
  },

  titleStyle: {
    color: colors.WHITE,
    fontFamily: fonts.SEMI_BOLD,
    fontSize: 15,
  },
});

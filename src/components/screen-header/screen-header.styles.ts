import {StyleSheet, TextStyle, ViewStyle} from 'react-native';
import {colors, fonts} from '../../theme';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.TRANSPARENT,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    marginTop: 16,
  } as ViewStyle,

  titleView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  } as ViewStyle,

  titleText: {
    fontFamily: fonts.SEMI_BOLD,
    fontSize: 20,
    color: colors.WHITE,
  } as TextStyle,

  LogoIcon: {
    width: 25,
    height: 25,
  } as ViewStyle,
});

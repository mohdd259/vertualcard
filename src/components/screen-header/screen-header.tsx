import React from 'react';
import {TouchableOpacity, View, ViewStyle, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {images} from '../../utils';
import Entypo from 'react-native-vector-icons/Entypo';
import {styles} from './screen-header.styles';
import {colors} from '../../theme';
import {CustomFastImage} from '../custom-fast-image';

interface ScreenHeaderProps {
  containerStyle?: ViewStyle;
  goBackButton?: boolean;
  leftComponent?: React.ReactNode;
  rightComponent?: React.ReactNode;
  title?: string;
}

export const ScreenHeader: React.FC<ScreenHeaderProps> = props => {
  const {
    containerStyle = {},
    goBackButton = true,
    leftComponent,
    rightComponent,
    title = '',
  } = props;
  const navigation = useNavigation();

  const getBackButtonComponent = () => (
    <View style={styles.titleView}>
      {goBackButton && (
        <>
          {leftComponent ? (
            leftComponent
          ) : (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Entypo name="chevron-thin-left" size={20} color={colors.WHITE} />
            </TouchableOpacity>
          )}
        </>
      )}
      {title !== '' && <Text style={styles.titleText}>{title}</Text>}
      {rightComponent ? (
        rightComponent
      ) : (
        <CustomFastImage
          reszieMode="contain"
          source={images.image_logo}
          style={styles.LogoIcon}
        />
      )}
    </View>
  );

  return (
    <View style={[styles.container, containerStyle]}>
      {getBackButtonComponent()}
    </View>
  );
};

import React from 'react';
import {
  useSafeAreaInsets,
  SafeAreaView,
  initialWindowMetrics,
  SafeAreaProvider,
} from 'react-native-safe-area-context';

export const CustomSafeAreaView = (props: any) => {
  return <SafeAreaView {...props}>{props.children}</SafeAreaView>;
};

export const CustomSafeAreaAssets = {
  useSafeAreaInsets,
  initialWindowMetrics,
  SafeAreaProvider,
};

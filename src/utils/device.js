import {Platform, Dimensions} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {getStatusBarHeight} from 'react-native-status-bar-height';

const {height, width} = Dimensions.get('window');

export const Device = {
  isAndroid: Platform.OS === 'android',
  isIOS: Platform.OS === 'ios',
  screenHeight: height,
  screenWidth: width,
  platform: Platform.OS,
  deviceId: DeviceInfo.getUniqueId(),
  statusbarHeight: getStatusBarHeight(),
};

import { showMessage } from "react-native-flash-message";
import { fonts } from '../theme'

export const actionServiceStatus = {
    SUCCESS: 'success',
    FAILED: 'failed',
    WARNING: 'warning',
    ERROR: 'error',
    DENIED: 'denied',
    GRANTED: 'granted'
}

export const genericErrorProblem = {
    NETWORK_ERROR: 'Network Error',
    BAD_REQUEST: 'Bad Request'
}

export const errorCode = {
    NETWORK_ERROR: "network-error",
    UN_MATCH_CREDENTIALS: 'un-matched-credentials'

}

export const snackbarMessageType = {
    SUCCESS: 'success',
    DANGER: 'danger',
    INFO: 'info',
    WARNING: 'warning'
}

export const errorMessage = {
    [errorCode.NETWORK_ERROR]: "No Internet, Please try again",
    [errorCode.UN_MATCH_CREDENTIALS]: 'Invalid Email or Password'
 
}


const modifyMessage = (message) => {
    if(message === genericErrorProblem.NETWORK_ERROR){
        message = errorMessage[errorCode.NETWORK_ERROR]
    }
    return message
} 

const makeUppercaseWords = ['otp']

const capitalizeString = (string) => {
    string = string.split('_').join(' ').split('-').join(' ').split(' ')
    return string.map(word => {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }).join(" ")
}

export const showSnackbar = (data, configs = {}) => {

    configs.snackbarType = configs.snackbarType || 'danger'
    configs.snackbarDuration = configs.snackbarDuration || 3000

    let message = errorMessage?.[data?.code || data] || data?.message || data
    message = modifyMessage(message)
    message = capitalizeString(message)
    
    //if needs to make a word to uppercase then fill the word into makeUppercaseWords array in lowercase
    message = message?.split(' ').map(a => {
        if (makeUppercaseWords.filter(x => a.toLowerCase()?.includes(x)).length > 0) {
            return a.toUpperCase()
        }
        return a
    }).join(' ')

    showMessage({
        message: message,
        type: configs.snackbarType,
        duration: configs.snackbarDuration,
        icon: configs.snackbarType,
        textStyle: { fontFamily: fonts.REGULAR },
        titleStyle:  { fontFamily: fonts.REGULAR },
    });
}
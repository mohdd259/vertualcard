import FastImage from 'react-native-fast-image'
import { Image } from 'react-native'
import { Device } from './device'

const getSource = (image) => ({ uri: Image.resolveAssetSource(image).uri })

export const images = {

  'image_account': getSource(require('../navigation/assets/account.png')),
  'image_credit': getSource(require('../navigation/assets/credit.png')),
  'image_home': getSource(require('../navigation/assets/home.png')),
  'image_pay': getSource(require('../navigation/assets/pay.png')),
  'image_payments': getSource(require('../navigation/assets/payments.png')),

  'image_logo': getSource(require('../components/screen-header/assets/logo.png')),
  'image_aspire_logo': getSource(require('../components/vertual-card/assets/aspire-logo.png')),
  'image_visa_logo': getSource(require('../components/vertual-card/assets/visa-logo.png')),
  'image_hide_eye': getSource(require('../components/vertual-card/assets/hide-eye.png')),
  'image_show_eye': getSource(require('../components/vertual-card/assets/show-eye.png')),


  'image_deactivate_card': getSource(require('../components/debit-card-feature-item/assets/deactivate-card.png')),
  'image_freeze_card': getSource(require('../components/debit-card-feature-item/assets/freeze-card.png')),
  'image_new_card': getSource(require('../components/debit-card-feature-item/assets/new-card.png')),
  'image_top_up': getSource(require('../components/debit-card-feature-item/assets/top-up.png')),
  'image_weekly_limit': getSource(require('../components/debit-card-feature-item/assets/weekly-limit.png')),

  'image_limit_meter': getSource(require('../screens/spending-limit/assets/limit-meter.png')),


}

export const initImages = () => {
    if (Device.isAndroid)
      return Promise.resolve(true)
  
    return new Promise((resolve) => {
      FastImage.preload(Object.values(images),
        () => { }, // onProgress
        (urls, loaded, skipped) => { // onComplete
          if (skipped > 0) {
            console.warn(`${skipped} images are not successfully preloaded.`)
          }
          resolve(true)
        })
  
    })
  }
export const isNumber = (value: any) => {
  return /^-?[\d.]+(?:e-?\d+)?$/.test(value)
}

export const addCommasToNumber = (value: number) => {
  const [result, decimal] = value.toString().split('.')
  const decimalValue = decimal ? `.${decimal}` : ''
  const others = result.substring(0, result.length - 3)
  let lastThree = result.substring(result.length - 3)
  if (others !== '') lastThree = ',' + lastThree
  return others.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + decimalValue
}


export const convertNumberToCommasKLC = (value: number|string, forceKLC = false, forceComma = false, forceRound = false, allowTrailingZeros = true) => {
  try {
    if (!isNumber(value)) {
      return value
    } else {
      value = +value
    }

    const isNegative = value < 0

    if (isNegative) {
      value = value * -1
    }

    if (forceRound) value = Math.floor(value)

    let finalValue: number|string

    if (value % 1 === 0) {
      value = Math.round(value)
    }

    const roundedValue = Math.floor(value)
    const digitCount = roundedValue.toString().length

    if (digitCount <= 2) {

      finalValue = (value % 1 === 0) ? value : allowTrailingZeros ? value.toFixed(2) : parseFloat(value.toFixed(2))

    } else if (digitCount === 3) {

      finalValue = roundedValue

    } else if ([4, 5].includes(digitCount)) {

      finalValue =  addCommasToNumber(roundedValue)

    } else {

      finalValue = addCommasToNumber(roundedValue)

    }

    return (isNegative ? "-" : "") + finalValue

  } catch (e) {
    return value
  }
}

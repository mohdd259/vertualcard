import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  Home,
  DebitCard,
  Payments,
  Credit,
  Profile,
  SpendingLimit,
} from '../screens';
import {getTabbar} from './custom-tabs/custom-bottom-home-tabs';

const Stack = createNativeStackNavigator();
const BottomTab = createBottomTabNavigator();

const DashboardStack = () => {
  return (
    <BottomTab.Navigator
      initialRouteName={'DebitCard'}
      screenOptions={{
        headerShown: false,
        tabBarItemStyle: {height: 56},
      }}>
      <BottomTab.Screen
        name="Home"
        component={Home}
        options={getTabbar('home')}
      />
      <BottomTab.Screen
        name="DebitCard"
        component={DebitCard}
        options={getTabbar('debitCard')}
      />
      <BottomTab.Screen
        name="Payments"
        component={Payments}
        options={getTabbar('payments')}
      />
      <BottomTab.Screen
        name="Credit"
        component={Credit}
        options={getTabbar('credit')}
      />
      <BottomTab.Screen
        name="Profile"
        component={Profile}
        options={getTabbar('profile')}
      />
    </BottomTab.Navigator>
  );
};

const MainStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="DashboardStack" component={DashboardStack} />
      <Stack.Screen name="SpendingLimit" component={SpendingLimit} />
    </Stack.Navigator>
  );
};

export const RootAppNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="MainStack" component={MainStack} />
    </Stack.Navigator>
  );
};

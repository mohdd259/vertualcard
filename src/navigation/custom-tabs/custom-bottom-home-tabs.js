import React from 'react';
import {Text, View} from 'react-native';
import {CustomFastImage} from '../../components';
import {colors, fonts} from '../../theme';
import {images} from '../../utils';

export const bottomHomeTabs = {
  home: {
    icon: images.image_home,
    lable: 'Home',
  },
  debitCard: {
    icon: images.image_pay,
    lable: 'Debit Card',
  },
  payments: {
    icon: images.image_payments,
    lable: 'Payments',
  },
  credit: {
    icon: images.image_credit,
    lable: 'Credit',
  },
  profile: {
    icon: images.image_account,
    lable: 'Profile',
  },
};

export const getTabbar = name => {
  return {
    tabBarLabel: ({focused}) => (
      <Text
        style={{
          fontSize: 9,
          fontFamily: focused ? fonts.SEMI_BOLD : fonts.MEDIUM,
          color: focused ? colors.GREEN00 : colors.GREYDD,
          marginBottom: 10,
        }}>
        {bottomHomeTabs[name].lable}
      </Text>
    ),
    tabBarIcon: ({focused}) => (
      <CustomFastImage
        source={bottomHomeTabs[name].icon}
        resizeMode="contain"
        style={{
          width: 24,
          height: 24,
        }}
      />
    ),
  };
};

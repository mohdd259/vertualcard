import { StyleSheet, TextStyle, ViewStyle } from "react-native";
import { colors, fonts } from "../../theme";
import { Device } from "../../utils";

const CARD_HEIGHT = 220;
const CARD_SHOW_HEIGHT = 33;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.BLUE0CA
  } as ViewStyle,

  debitCardText: {
    fontFamily: fonts.BOLD,
    fontSize: 24,
    color: colors.WHITE,
    marginTop: 15
  } as TextStyle,

  availableBalanceText: {
    fontFamily: fonts.MEDIUM,
    fontSize: 14,
    color: colors.WHITE,
    marginTop: 24,
    marginBottom: 10
  } as TextStyle,

  currencyView: {
    backgroundColor: colors.GREEN00,
    width: 40,
    height: 22,
    borderRadius: 2,
    justifyContent: "center",
    alignItems: "center"
  } as ViewStyle,

  currencyText: {
    fontFamily: fonts.BOLD,
    fontSize: 12,
    color: colors.WHITE,
  } as TextStyle,

  balanceText: {
    fontFamily: fonts.BOLD,
    fontSize: 24,
    color: colors.WHITE,
    marginLeft: 15
  } as TextStyle,

  contentWrapper: {
    position: "absolute",
    height: Device.screenWidth * .35,
    width: "100%",
    zIndex: 0,
  } as ViewStyle,

  contentView: {
    paddingHorizontal: 20,
  } as ViewStyle,

  currencyWrapper: {
    flexDirection: "row",
    alignItems: "center"
  } as ViewStyle,

  debitcardInfoWrapper: {
    flex: 1,
  } as ViewStyle,

  contentContainer: {
    flex: 1,
    marginTop: Device.screenWidth * 0.35 + 20,
  } as ViewStyle,

  cardInfoView: {
    flex: 1,
    backgroundColor: colors.WHITE,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginTop: (CARD_HEIGHT + CARD_SHOW_HEIGHT) / 2.2,
    paddingHorizontal: 20,
    paddingTop: CARD_HEIGHT / 1.5,
  } as ViewStyle,

  cardInfoContentContainer: {
    backgroundColor: colors.WHITE,
  } as ViewStyle,

  vertualCardWrapper: {
    position: 'absolute',
    zIndex: 100,
    width: '100%',
    justifyContent: 'center',
  } as ViewStyle,
});

import {View, Text} from 'react-native';
import React from 'react';
import {styles} from './debit-card.styles';
import {
  DebitCardFeatureItem,
  ScreenHeader,
  VertualCard,
  DebitCardLimitBar,
} from '../../components';
import {ScrollView} from 'react-native-gesture-handler';
import {convertNumberToCommasKLC} from '../../utils';
import {useSelector} from 'react-redux';
import {DebitcardData} from '../../redux/debitcard/types';
import type {RootState} from '../../redux/rootReducer';

interface DebitCardProps {
  navigation: string;
  route: any;
}

export const DebitCard: React.FC<DebitCardProps> = props => {
  const {} = props;
  const debitcard: DebitcardData = useSelector(
    (state: RootState) => state.debitcard,
  );
  const {userBalance, debitCardFeatures} = debitcard;

  const getLeftComponent = () => {
    return <Text style={styles.debitCardText}>Debit Card</Text>;
  };

  return (
    <View style={styles.container}>
      <View style={styles.contentWrapper}>
        <ScreenHeader leftComponent={getLeftComponent()} />
        <View style={styles.contentView}>
          <Text style={styles.availableBalanceText}>Available balance</Text>
          <View style={styles.currencyWrapper}>
            <View style={styles.currencyView}>
              <Text style={styles.currencyText}>$$</Text>
            </View>
            <Text style={styles.balanceText}>
              {convertNumberToCommasKLC(userBalance, true, true)}
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.debitcardInfoWrapper}>
        <ScrollView>
          <View style={styles.contentContainer}>
            <View style={styles.vertualCardWrapper}>
              <VertualCard />
            </View>
            <View style={styles.cardInfoView}>
              <DebitCardLimitBar />
              {debitCardFeatures.map((item, index: number) => {
                return (
                  <View
                    key={String(index)}
                    style={[index === 0 && {marginTop: 34}]}>
                    <DebitCardFeatureItem {...item} />
                  </View>
                );
              })}
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

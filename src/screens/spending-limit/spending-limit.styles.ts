import { ImageStyle, StyleSheet, TextStyle, ViewStyle } from "react-native";
import { colors, fonts } from "../../theme";
import { Device } from "../../utils";


export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.BLUE0CA
  } as ViewStyle,

  screenTitle: {
    fontFamily: fonts.BOLD,
    fontSize: 24,
    color: colors.WHITE,
    marginLeft: 20,
    marginTop: 15
  } as TextStyle,

  infoContainer: {
    flex: 1,
    backgroundColor: colors.WHITE,
    marginTop: 40,
    paddingTop: 30,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 24,
    paddingHorizontal: 20,
    paddingBottom: Device.statusbarHeight
  } as ViewStyle,

  meterImage: {
    width: 16,
    height: 16,
    marginRight: 16
  } as ImageStyle,

  setLimitText: {
    fontSize: 13,
    fontFamily: fonts.MEDIUM,
    color: colors.BLACK22
  } as TextStyle,

  limitWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 13
  } as ViewStyle,

  currencyWrapper: {
    flexDirection: "row",
    alignItems: "center"
  } as ViewStyle,

  currencyView: {
    backgroundColor: colors.GREEN00,
    width: 40,
    height: 24,
    borderRadius: 2,
    justifyContent: "center",
    alignItems: "center"
  } as ViewStyle,

  currencyText: {
    fontFamily: fonts.BOLD,
    fontSize: 12,
    color: colors.WHITE
  } as TextStyle,

  balanceText: {
    fontFamily: fonts.BOLD,
    fontSize: 24,
    color: colors.WHITE,
    marginLeft: 15
  } as TextStyle,

  limitNoteText: {
    fontSize: 11,
    fontFamily: fonts.REGULAR,
    color: colors.GREY226,
    marginTop: 12
  } as TextStyle,

  contentView: {
    flex: 1
  } as ViewStyle,

  buttonStyle: {
    marginHorizontal: 20
  } as ViewStyle,

  defaultLimitButton: {
    width: Device.screenWidth * .26,
    height: 40,
    borderRadius: 3,
    backgroundColor: colors.GREENE5F,
    marginRight: 10
  } as ViewStyle,

  defaultLimitButtonWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 32
  } as ViewStyle,

  defaultButtonText: {
    color: colors.GREEN00,
    fontSize: 12,
    fontFamily: fonts.SEMI_BOLD
  },

  customInput: {
    fontFamily: fonts.SEMI_BOLD,
    fontSize: 24
  } as TextStyle
});

import {View, Text} from 'react-native';
import React, {useState} from 'react';
import {styles} from './spending-limit.styles';
import {
  CustomButton,
  CustomFastImage,
  CustomInput,
  ScreenHeader,
} from '../../components';
import {convertNumberToCommasKLC, images} from '../../utils';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {
  setWeeklyLimitRequest,
  setWeeklyLimitStatusRequest,
} from '../../redux/actions';

const defaultSpendingLimits = [
  {lable: 'S$ 5,000', limit: '5000'},
  {lable: 'S$ 10,000', limit: '10000'},
  {lable: 'S$ 20,000', limit: '20000'},
];

interface SpendingLimitProps {}

export const SpendingLimit: React.FC<SpendingLimitProps> = props => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [limit, setLimit] = useState('');

  const handleSetWeeklyLimit = () => {
    dispatch(setWeeklyLimitRequest(Number(limit)));
    dispatch(setWeeklyLimitStatusRequest(true));
    navigation.goBack();
  };

  const isValue = Number(limit.trim().split(',').join('')) > 0;

  const handleChange = (value: string) => {
    const limitValue = value.trim().split(',').join('');
    setLimit(limitValue);
  };

  return (
    <View style={styles.container}>
      <ScreenHeader />
      <Text style={styles.screenTitle}>Spending limit</Text>
      <View style={styles.infoContainer}>
        <View style={styles.contentView}>
          <View style={styles.limitWrapper}>
            <CustomFastImage
              resizeMode="contain"
              source={images.image_limit_meter}
              style={styles.meterImage}
            />
            <Text style={styles.setLimitText}>
              Set a weekly debit card spending limit
            </Text>
          </View>
          <CustomInput
            value={convertNumberToCommasKLC(limit, true, true)}
            onChangeText={handleChange}
            leftIcon={
              <View style={styles.currencyWrapper}>
                <View style={styles.currencyView}>
                  <Text style={styles.currencyText}>$$</Text>
                </View>
              </View>
            }
            onlyNumericAllowed={true}
            allowKeywords={[',']}
            style={styles.customInput}
          />
          <Text style={styles.limitNoteText}>
            Here weekly means the last 7 days - not the calendar week
          </Text>
          <View style={styles.defaultLimitButtonWrapper}>
            {defaultSpendingLimits.map((item, index) => {
              return (
                <CustomButton
                  key={String(index)}
                  titleStyle={styles.defaultButtonText}
                  title={item.lable}
                  buttonStyle={styles.defaultLimitButton}
                  onPress={() => setLimit(item.limit)}
                />
              );
            })}
          </View>
        </View>
        <CustomButton
          disabled={!isValue}
          title="Save"
          onPress={handleSetWeeklyLimit}
          buttonStyle={styles.buttonStyle}
        />
      </View>
    </View>
  );
};

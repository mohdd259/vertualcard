import {View, Text} from 'react-native';
import React, {useCallback} from 'react';
import {CustomLoader} from '../../components';
import {styles} from './home.styles';
import {ScreenHeader} from '../../components/screen-header';
import {useDispatch, useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {mokeApiRequest} from '../../redux/actions';
import {RootState} from '../../redux/rootReducer';
import {MokeDataTypes} from '../../redux/debitcard/types';

interface HomeProps {}

export const Home: React.FC<HomeProps> = props => {
  const dispatch = useDispatch();
  const mokedata: MokeDataTypes = useSelector(
    (state: RootState) => state.debitcard.mokeData,
  );

  const {isLoading} = mokedata;

  const fetchProductDetails = () => {
    dispatch(mokeApiRequest());
  };

  useFocusEffect(
    useCallback(() => {
      fetchProductDetails();
    }, []),
  );

  return (
    <View style={styles.container}>
      <ScreenHeader title={'Mock Api'} leftComponent={<View />} />
      {isLoading ? (
        <CustomLoader isLoading={isLoading} />
      ) : (
        <View style={styles.contentView}>
          <Text style={styles.noteText}>
            Here we called a mock api {'\n'} as per assignment, Thanks :)
          </Text>
        </View>
      )}
    </View>
  );
};

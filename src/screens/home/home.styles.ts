import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../theme';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.BLUE0CA,
  },
  contentView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noteText: {
    color: colors.WHITE,
    fontSize: 15,
    fontFamily: fonts.SEMI_BOLD,
    textAlign: 'center',
  },
});

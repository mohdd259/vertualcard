export * from './static-config';

export const configs = {
  BASE_URL: 'https://reqres.in/api',
};

export const API_CONFIG = {
  timeout: 10000,
  headers: {
    common: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  },
};

import {GetProduct} from '../api-invoker/get-product-details';

export class Api {
  static async getProduct() {
    return new GetProduct().invoke();
  }
}

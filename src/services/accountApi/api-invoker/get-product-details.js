import ApiService from '../../apiService';
import {apiConstants} from '../network';

export class GetProduct {
  /**
   * Function to invoke GET API to fetch product details
   */
  async invoke() {
    const result = await ApiService.getInstance().get(apiConstants.PRODUCT);
    return result;
  }
}

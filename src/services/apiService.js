import * as ApiConstants from '../configs';
import axios from 'axios';

class ApiService {

    constructor(authToken = '') {
        this.axiosClient = axios.create({ ...ApiConstants.API_CONFIG, baseURL: ApiConstants.configs.BASE_URL })
      }

      static init = (rootStore) => {
        ApiService.rootStore = rootStore
        return new ApiService()
      }

      static getInstance = (token = null) => {
       //here set some data in ApiService class
        return new ApiService()
      }


      get = async (apiPath, options) => {

        const source = axios.CancelToken.source()
        const timeout = setTimeout(source.cancel, options?.extras?.timeout || ApiConstants.API_CONFIG.timeout)

        return await this.axiosClient
          .get(apiPath, {
            baseURL: options?.baseURL,
            cancelToken: source.token,
            transformRequest: [
              (data, headers) => {
                return options?.transformRequest ? options.transformRequest(data, headers) : data
              },
            ],
            /*
                validateStatus returns the response in case of error
            */
            validateStatus: () => {
              return true
            },
            ...options?.extras

          }).then((response) => {
            clearTimeout(timeout)
            return response
          }).catch((actualError) => {
            clearTimeout(timeout)
            if (actualError.name === 'TypeError') {

              const customError = new Error('Network Error')
              customError.name = 'APIError'
              throw customError

            } else {
              throw actualError
            }
          })
      }


      post = async (apiPath, payload, options) => {
        const source = axios.CancelToken.source()
        const timeout = setTimeout(source.cancel, options?.extras?.timeout || ApiConstants.API_CONFIG.timeout)

        return await this.axiosClient
          .post(apiPath, JSON.stringify(payload), {
            baseURL: options?.baseURL,
            cancelToken: source.token,
            headers: this.axiosClient.defaults.headers.common,
            transformRequest: [
              (data, headers) => {
                return options?.transformRequest ? options.transformRequest(data, headers) : data
              },
            ],
            /*
                validateStatus returns the response in case of error
            */
            validateStatus: () => {
              return true
            },
          }).then((response) => {
            clearTimeout(timeout)
            return response
          }).catch((actualError) => {
            clearTimeout(timeout)
            if (actualError.name === 'TypeError') {

              const customError = new Error('Network Error')
              customError.name = 'APIError'
              throw customError

            } else {
              throw actualError
            }
          })
      }


      put = async (apiPath, payload, options) => {
        const source = axios.CancelToken.source()
        const timeout = setTimeout(source.cancel, options?.extras?.timeout || ApiConstants.API_CONFIG.timeout)

        return await this.axiosClient
          .put(apiPath, JSON.stringify(payload), {
            baseURL: options?.baseURL,
            cancelToken: source.token,
            headers: this.axiosClient.defaults.headers.common,
            transformResponse: (res) => (responseTransformer(res, options?.transformResponse)),
            /*
             validateStatus returns the response in case of error
            */
            validateStatus: () => {
              return true
            },
          }).then((response) => {
            clearTimeout(timeout)
            return response
          }).catch((actualError) => {
            clearTimeout(timeout)
            if (actualError.name === 'TypeError') {

              const customError = new Error('Network Error')
              customError.name = 'APIError'
              throw customError

            } else {
              throw actualError
            }
          })
      }

      delete = async (apiPath) => {
        return await this.axiosClient.delete(apiPath).then((response) => response)
      }
}


export default ApiService

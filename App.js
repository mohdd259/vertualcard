/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useRef, useState} from 'react';
import {StatusBar, StyleSheet} from 'react-native';
import {RootAppNavigator} from './src/navigation/app-navigator';
import {colors, initTheme} from './src/theme';
import {catchFiredApiInNetwork, initImages} from './src/utils';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import {CustomSafeAreaAssets, CustomSafeAreaView} from './src/components';
import {ThemeProvider} from 'react-native-elements';
import FlashMessage from 'react-native-flash-message';

catchFiredApiInNetwork(); // see the fired api in browser network

const App = () => {
  const rootThemeRef = useRef({});
  let rootTheme = rootThemeRef.current;
  const initialRouteName = useRef('');
  const [isRenderReady, setRenderReady] = useState(false);

  const appInitialize = async () => {
    setRenderReady(true);
    rootTheme = rootThemeRef.current = await initTheme();
    await initImages();
  };

  useEffect(() => {
    appInitialize().then();
  }, []);

  return (
    <ThemeProvider theme={rootTheme}>
      <CustomSafeAreaAssets.SafeAreaProvider
        initialSafeAreaInsets={
          CustomSafeAreaAssets.initialWindowMetrics?.insets || {
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
          }
        }>
        {isRenderReady && (
          <CustomSafeAreaView style={styles.container} edges={['top']}>
            <StatusBar
              barStyle={'light-content'}
              backgroundColor={colors.BLUE0CA}
            />
            <Provider store={store}>
              <NavigationContainer>
                <RootAppNavigator initialRouteName={initialRouteName.current} />
              </NavigationContainer>
            </Provider>
            <FlashMessage position="top" />
          </CustomSafeAreaView>
        )}
      </CustomSafeAreaAssets.SafeAreaProvider>
    </ThemeProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.BLUE0CA,
  },
});

export default App;

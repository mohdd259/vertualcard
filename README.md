# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A way to set your debit card limit and access their details.

Currently includes:

- React Native
- React Navigation
- Redux / Redux-saga (State Management)
- TypeScript 
- Montserrat fonts (Note: I did not find Avenir Next fonts so i used instead)
- And more!
* [Find project link on bitbucket](https://bitbucket.org/mohdd259/vertualcard)

### How do I get set up? ###
* Setup android studio for android/ xcode for ios
* Open the project and do run `npm install` on the root of the project
* Then run `react-native run-android` for `android`/ `react-native run-ios` for `ios`
* Or You  can run the app using `Android Studio` for `android` / `Xcode` for `ios`


### Project Structure ###

* src
* components
* configs
* navigation
* redux 
* screens 
* services
* theme
* utils
* App.js

**components**
This is where your React components will live. Each component will have a directory containing the `.tsx` file. The app will come with some commonly used components like Button.

**navigation**
This is where your `react-navigation` navigators will live.

**redux**
This is where your app's models will live. Each model has a directory which will contain the `react-redux` model file, test file, and any other supporting files like actions, types, etc.


**screens**
This is where your screen components will live. A screen is a React component which will take up the entire screen and be part of the navigation hierarchy. Each screen will have a directory containing the `.tsx` file, along with any assets or other helper files.

**theme**
Here lives the theme for your application, including spacing, colors, and typography.

**utils**
This is a great place to put miscellaneous helpers and utilities. Things like date helpers, formatters, etc. are often found here. However, it should only be used for things that are truely shared across your application. If a helper or utility is only used by a specific component or model, consider co-locating your helper with that component or model.
### Who do I talk to? ###

**App.js** This is the entry point to your app. This is where you will find the main App component which renders the rest of the application.

* Repo owner: Mohammad Danish
* Contact: mohdd259@gmail.com
